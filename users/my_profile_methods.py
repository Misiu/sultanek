__author__ = 'Vardens'
"""
Created on 2015-03-01

@author: Vardens
"""


def change_my_password(request):
    if request.user.check_password(request.POST['password']):
        if request.POST['new_password'] == request.POST['confirm_password']:
            request.user.set_password(request.POST['new_password'])
            request.user.save()
            return {'message': "Udało się zmienić hasło!", 'success': True}
        else:
            return {'message': "Hasła się nie zgadzają", 'success': False}
    else:
        return {'message': "Niepoprawne stare hasło!", 'success': False}


def change_my_note(request):
    pass


def change_my_sex(request):
    pass


def change_my_birthdate(request):
    pass