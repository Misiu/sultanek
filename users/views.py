from django.shortcuts import render
from django.contrib.auth.models import User
from users.models import MyUser
from django.contrib.auth import authenticate, login
import datetime
from users.models import Comment
from users.my_profile_methods import *


def register_page(request):
    return render(request, 'users/register_page.html', {'all_news': None})


def register_method(request):
    errors = []
    if request.POST.get('password', 1) != request.POST.get('confirm_password', 2):
        errors.append('wrong_password')

    if (request.POST.get('username', "field is empty") == "" or request.POST.get('email',
                                                                                 "field is empty") == "" or
                request.POST.get(
                        'password', "field is empty") == ""):
        errors.append('empty_field')

    if len(errors) != 0:
        return render(request, 'users/register_page.html', {'errors': errors})

    else:
        User.objects.create_user(username=request.POST['username'], email=request.POST['email'],
                                 password=request.POST['password'])
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        login(request, user)
        my_user = MyUser(user=user, birthdate=datetime.datetime.strptime(request.POST['birthdate'], "%d/%m/%Y"))
        my_user.save()

        return render(request, 'base/index.html')


def my_profile(request):
    if request.user.is_authenticated():
        # print("TOOO: "+str(
        # request.user.original_user.note     #))
        if request.POST.get('new_password'):
            results = change_my_password(request)

            return render(request, 'users/my_profile.html', results)

            # my_user = MyUser.objects.get(user=request.user)
            #
            # if 'birthdate' in request.POST.keys() and (request.POST.get('birthdate') != my_user.birthdate):
            # results = change_my_birthdate(request)
            # return render_to_response('users/my_profile.html', results, RequestContext(request))
    else:
        return render(request, 'base/index.html')
    return render(request, 'users/my_profile.html')


def my_comments(request):
    # try:
    if 'query' in request.POST:

        my_comments_list = Comment.objects.filter(user=request.user, comment__contains=request.POST['query']).order_by(
            'date')
    else:
        my_comments_list = Comment.objects.filter(user=request.user).order_by('date')
    # except:
    # my_comments_list = []
    # for i in range(10):
    # my_comments_list.append(comm("komentarz"+str(i)))

    return render(request, 'users/my_comments.html', {'my_comments': my_comments_list})