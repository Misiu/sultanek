from django.db import models
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from locations.models import Location


class Achievement(models.Model):
    name = models.CharField(max_length=200, null=False)
    description = models.CharField(max_length=500, null=True)
    # TODO requirements


class MyUser(models.Model):
    user = models.OneToOneField(User, related_name='my_user')
    birthdate = models.DateField(null=True)
    sex = models.CharField(max_length=1, null=True)
    note = models.CharField(max_length=500, null=True)


# TODO zastanowić się jakie uprawnienia
# Permission.objects.create(codename='can_add_new_place',
#                           name='Dodawanie nowego miejsca')
# Permission.objects.create(codename='can_add_comment',
#                           name='Dodawanie komentarzy')


class Comment(models.Model):
    user = models.ForeignKey(User)
    value = models.IntegerField(null=True, default=0)
    comment = models.CharField(max_length=500)
    date = models.DateField(null=False)
    location = models.ForeignKey(Location, null=True)