from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login


def index(request):
    return render(request, 'base/index.html', {'all_news': None})


def logout_method(request):
    logout(request)
    return index(request)


def login_method(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return render(request, 'users/login_page.html', {'login_problem': True})
            #return render(request, 'base/index.html', {'login_problem': True})
    else:
        # Return an 'invalid login' error message.
        return render(request, 'users/login_page.html', {'login_problem': True})
#        return render(request, 'base/index.html', {'login_problem': True})


def login_page(request):
    pass