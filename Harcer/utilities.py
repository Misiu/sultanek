'''
Created on 21-09-2014

@author: Vardens
'''
import datetime as dt

def utilities(request):
    return {'is_winter': is_winter(),
            'is_autumn': is_autumn(),
            'is_day': is_day(), 
            }
    
    
def is_winter():
    '''checks if we have december or january'''
    if(dt.date.today().month==1 or dt.date.today().month==12):
        return True
    else:
        return False
    
def is_autumn():
    '''check is leaves should be falling'''
    if(dt.date.today().month==10 or dt.date.today().month==11):
        return True
    else:
        return False
    
def is_day():
    '''if  minutes are dividable by 2, then set day'''
    if(dt.datetime.now().minute % 2==0):
        return True
    else:
        return False
    
    
    
    