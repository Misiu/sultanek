from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'page.views.index', name='home'),

                       url(r'^location_detail/(?P<location_id>\d+)', 'locations.views.location_detail'),
                       url(r'^locations/$', 'locations.views.locations'),
                       url(r'^location_advanced/$', 'locations.views.location_advanced'),
                       url(r'^location_adding/$', 'locations.views.location_one'),

                       url(r'^admin/', include(admin.site.urls)),
                       #url(r'^login/$', 'django.contrib.auth.views.login'),
                       url(r'^login/$', 'page.views.login_method'),
                       url(r'^logout/$', 'page.views.logout_method'),
                       url(r'^register/$', 'users.views.register_page'),
                       url(r'^register_process/$', 'users.views.register_method'),

                       url(r'^my_comments/$', 'users.views.my_comments'),
                       url(r'^my_profile/$', 'users.views.my_profile'),

                       url(r'^change_my_birthdate/$', 'users.views.change_my_birthdate'),
                       url(r'^change_my_password/$', 'users.views.change_my_password'),
                       url(r'^change_my_note/$', 'users.views.change_my_note'),

)
