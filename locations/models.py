from django.db import models
from django.conf import settings

class Destination(models.Model):
    name = models.CharField(max_length=100, null=False)
    
class LocationType(models.Model):
    name = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=100, null=True)
class Property(models.Model):
    
    shortcut=models.CharField(max_length=200, null=False)
    name=models.CharField(max_length=200, null=False)
#   property_existance=models.BooleanField(null=False, default=False)
    optional_answer=models.CharField(max_length=200, null=True)
    
class Location(models.Model):
    # podstawowe
    name = models.CharField(max_length=200, null=False)

    # Lokalizacja
    latitude = models.FloatField(null=False)
    longitude = models.FloatField(null=False)
    address = models.CharField(max_length=300, null=False)
    street = models.CharField(max_length=100, null=True)
    voivodeship = models.CharField(max_length=100, null=True)
    city = models.CharField(max_length=100, null=True)
    # Typ
#     TYPE_NAME_CHOICES = ('basen', 'drzewo', 'pomnik', 'muzeum')
    flag_property=models.ManyToManyField(Property, null=True)

    type_name = models.ManyToManyField(LocationType)
    destination = models.ManyToManyField(Destination, null=True)
    size = models.IntegerField(null=True, default=0)
#     private_property = models.BooleanField(null=False, default=False)

    # Flags
#     wc = models.BooleanField(null=False, default=False)
#     sleeping = models.BooleanField(null=False, default=False)
#     own_food = models.BooleanField(null=False, default=False)

